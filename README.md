# Manjaro-Plasma-Reset

Reset Plasma to Manjaro Installed Default

## Get script

    git clone https://gitlab.manjaro.org/applications/manjaro-plasma-reset.git

## Usage
Plasma caches the configurations and will rewrite them on logout.

It is therefore necessary to execute the script outside a plasma session.

1. Logout
2. Switch to a TTY e.g. TTY4 <kbd>Ctrl</kbd><kbd>Alt</kbd><kbd>F4</kbd>
3. Login as your user
4. Run the script
```
bash manjaro-plasma-reset/manjaro-plasma-reset.sh
```
After the userspace restart you can log back in.